#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for Pcap to English
# Marelle Leon, April 2023

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=$(basename "$0")

function tst() {
  if [ "$#" -ne 3 ]; then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$3" "$1"
}

tst "E1" "src foo" "a source is foo" 

tst "E2" "host foo" "a host is foo"

tst "E3" "src host foo" "a source host is foo"

tst "E4" "ip" "of type IPv4"

tst "E5" "ip src foo" "IPv4 that has a source of foo"

tst "E6" "ip host foo" "IPv4 that has a host of foo"

tst "E7" "ip src host foo" "IPv4 that has a source host of foo"

tst "E8" "! ip" "not of type IPv4"

tst "E9" "! src foo" "a source is not foo"

tst "E10" "! host foo" "a host is not foo"

tst "E11" "! src host foo" "a source host is not foo"

tst "E12" "! ip src foo" "IPv4 that does not have a source of foo"

tst "E13" "! ip host foo" "IPv4 that does not have a host of foo"

tst "E14" "! ip src host foo" "IPv4 that does not have a source host of foo"

tst "E15" "port ftp-data" "a port is ftp-data"

tst "E16" "dst port ftp-data" "a destination port is ftp-data"

tst "E17" "tcp" "of type tcp"

tst "E18" "tcp port ftp-data" "tcp that has a port of ftp-data"

tst "E19" "tcp dst port ftp-data" "tcp that has a destination port of ftp-data"

tst "E20" "portrange 6000-6008" "a port range is 6000-6008"

tst "E21" "net 128.3" "a network is 128.3"

tst "E22" "port 20" "a port is 20"

tst "E23" "src or dst port ftp-data" "a source or destination port is ftp-data"

tst "E24" "ether src foo" "ethernet that has a source of foo"

tst "E25" "arp net 128.3" "arp that has a network of 128.3"

tst "E26" "tcp port 21" "tcp that has a port of 21"

tst "E27" "udp portrange 7000-7009" "udp that has a port range of 7000-7009"

tst "E28" "ether host 00:02:03:04:05:06" "ethernet that has a host of 00:02:03:04:05:06"

tst "E29" "ip src foo || arp src foo || rarp src foo" "IPv4 that has a source of foo, or (arp that has a source of foo, or rarp that has a source of foo)"

tst "E30" "ip net bar || arp net bar || rarp net bar" "IPv4 that has a network of bar, or (arp that has a network of bar, or rarp that has a network of bar)"

tst "E31" "tcp port 53 || udp port 53" "tcp that has a port of 53, or udp that has a port of 53"

tst "E32" "host foo && ! port ftp && ! port ftp-data" "a host is foo, and (a port is not ftp, and a port is not ftp-data)"

tst "E33" "host \\host" "a host is host"

tst "E34" "src host \\host" "a source host is host"

tst "E35" "ip host \\host" "IPv4 that has a host of host"

tst "E36" "ip src host \\host" "IPv4 that has a source host of host"

tst "E37" "ip[0] = 1" "{ # the 0th byte of the IPv4 header } is equal to { 1 }"

tst "E38" "tcp[0] = 1" "{ # the 0th byte of the tcp header } is equal to { 1 }"

tst "E39" "ip[1] = 1" "{ # the first byte of the IPv4 header } is equal to { 1 }"

tst "E40" "ip[0 : 2] = 1" "{ # the 0th 2 bytes of the IPv4 header } is equal to { 1 }"

tst "E41" "ip[0 : 4] = 1" "{ # the 0th 4 bytes of the IPv4 header } is equal to { 1 }"

tst "E42" "ip[0] = (0 + 1)" "{ # the 0th byte of the IPv4 header } is equal to { 0 + 1 }"

tst "E43" "ip[0] = (ip[0] | (0 + 1))" "examining the 0th byte of the IPv4 header: { # it } is equal to { # it | (0 + 1) }"

tst "E44" "ip[0] = ((0 + 1) | ip[0])" "examining the 0th byte of the IPv4 header: { # it } is equal to { (0 + 1) | # it }"

tst "E45" "ip[0] = ((ip[0] + 1) | 1)" "examining the 0th byte of the IPv4 header: { # it } is equal to { (# it + 1) | 1 }"

tst "E46" "ip[0] = ((1 + ip[0]) | 1)" "examining the 0th byte of the IPv4 header: { # it } is equal to { (1 + # it) | 1 }"

tst "E47" "ether proto \ip && host \host" "ethernet that has a proto of IPv4, and a host is host"

tst "E48" "(tcp[tcpflags] & (tcp-syn | tcp-fin)) != 0" "{ tcp[tcpflags] & (tcp-syn | tcp-fin) } does not equal { 0 }"

tst "E49" "tcp dst port ftp || ftp-data || domain" "tcp that has a destination port which is one of [ftp, ftp-data, domain]"

tst "E50" "port ftp || ftp-data || domain" "a port is one of [ftp, ftp-data, domain]"

tst "E51" "! (port ftp || ftp-data)" "a port is not one of [ftp, ftp-data]"

tst "E52" "host 0:0:0:0:0:0:0:1" "a host is 0:0:0:0:0:0:0:1"

tst "E53" "ether proto \\ip && host \\host" "ethernet that has a proto of IPv4, and a host is host"

tst "E54" "gateway 1" "a gateway is 1"

tst "E55" "dst net 172.16 mask 255.255.0.0" "a destination network is 172.16 with a mask of 255.255.0.0"

tst "E56" "! dst net 172.16 mask 255.255.0.0" "a destination network is not 172.16 with a mask of 255.255.0.0"

tst "E57" "dst net 172.16/16" "a destination network is 172.16 of length 16 bits"

tst "E58" "dst net 10/8" "a destination network is 10 of length 8 bits"

tst "E59" "ip broadcast" "IPv4 that is broadcast"

tst "E60" "icmp || udp port 53 || bootpc" "of type icmp, or udp that has a port which is one of [53, bootpc]"

tst "E61" "ip && (udp[8:2] = 0x1111) || (icmp[8:2] = 0x1111) || (tcp[((tcp[12]&0xf0)>>2):2] = 0x1111)" "(of type IPv4, and { # the eighth 2 bytes of the udp header } is equal to { 0x1111 }), or ({ # the eighth 2 bytes of the icmp header } is equal to { 0x1111 }, or { # the (tcp[12] & 0xf0) >> 2th 2 bytes of the tcp header } is equal to { 0x1111 })"

tst "E62" "tcp port 80 && (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)" "tcp that has a port of 80, and { (ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 } does not equal { 0 }"

tst "E63" "ip6 dst host fe80:0000:0000:0000:1111:11ff:fe11:1111" "IPv6 that has a destination host of fe80:0000:0000:0000:1111:11ff:fe11:1111"

tst "E64" "port 80 && tcp[((tcp[12:1] & 0xf0) >> 2):4] = 0x12345678" "a port is 80, and { # the (tcp[12 : 1] & 0xf0) >> 2th 4 bytes of the tcp header } is equal to { 0x12345678 }"