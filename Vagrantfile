# VM setup to build Caper on Debian 11
# Nik Sultana, Illinois Tech, August 2022

Vagrant.configure("2") do |config|
  config.vm.box = "debian/bullseye64"

  config.vm.box_check_update = false

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "8192"]
    vb.customize ["modifyvm", :id, "--cpus", "4"]
    vb.customize ["modifyvm", :id, "--hwvirtex", "on"]
    vb.customize ["modifyvm", :id, "--audio", "none"]
    vb.customize ["modifyvm", :id, "--nictype1", "virtio"]
    vb.customize ["modifyvm", :id, "--nictype2", "virtio"]
  end

  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    sudo apt-get install m4 -y
    sudo apt-get install opam -y
    opam init -y
    eval $(opam env)
    opam install ocamlfind
    opam install ocamlbuild
    opam install menhir -y
    git clone https://gitlab.com/niksu/caper
    cd caper
    ./build.sh caper.native
    file _build/caper.native
  SHELL

end
