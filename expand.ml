(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Expansion for pcap expressions
*)

open Pcap_syntax
open Pcap_syntax_aux
open Names


(** Fill in as much info in a primitive as possible **)

(*This rewrites a primitive into a form that's more detailed.*)
let rec expand_prim
 ((prims, acc) : primitive list * primitive list) : primitive list * primitive list =
   match prims with
   | [] -> ([], acc)
   | prim :: rest ->
       begin
       match prim with

       (*Expand out "tcp" etc, and remove the original atom.*)
       | ((Some proto, None, None), Nothing) when proto = Tcp || proto = Udp ->
           let prims =
             [((Some Ip, None, Some Proto), Escaped_String (string_of_proto proto));
              ((Some Ip6, None, Some Proto), Escaped_String (string_of_proto proto))] in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> [" ^
             begin
               List.map (fun prim -> Pcap_syntax_aux.string_of_pcap_expression (Primitive prim)) prims
               |> String.concat "; "
             end ^
             "]" ^ "\n");
           expand_prim (prims @ rest, acc)

       | ((Some (Icmp as proto), None, None), Nothing) ->
           let prims =
             [((Some Ip, None, Some Proto), Escaped_String (string_of_proto proto))] in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> [" ^
             begin
               List.map (fun prim -> Pcap_syntax_aux.string_of_pcap_expression (Primitive prim)) prims
               |> String.concat "; "
             end ^
             "]" ^ "\n");
           expand_prim (prims @ rest, acc)

       | ((Some (Icmp6 as proto), None, None), Nothing) ->
           let prims =
             [((Some Ip6, None, Some Proto), Escaped_String (string_of_proto proto))] in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> [" ^
             begin
               List.map (fun prim -> Pcap_syntax_aux.string_of_pcap_expression (Primitive prim)) prims
               |> String.concat "; "
             end ^
             "]" ^ "\n");
           expand_prim (prims @ rest, acc)

       | ((None, None, Some Proto), Escaped_String proto) -> (*FIXME unsure about this rewrite, maybe should remove*)
           (*NOTE in this case we're rewriting something like "proto \tcp" to
                  "tcp", and it can be argued that we're doing the opposite
                  of "expanding", but this is simply a convenient rewrite to
                  turn "proto \tcp" into a form that other rules can match
                  and process.*)
           let prim' = ((Some (proto_of_string proto), None, None), Nothing) in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim') ^ "\n");
           expand_prim (prim' :: rest, acc)

       | ((proto, None, None), Nothing) when proto <> None ->
           Aux.diag_output ("INFO: expand_prim no : " ^ Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^ "\n");
           expand_prim (rest, prim :: acc)
       | ((proto, None, typ), v) when typ <> Some Proto &&
             proto <> Some Vlan && proto <> Some Mpls &&
             typ <> Some Protochain ->
           let prim' = ((proto, Some Src_or_dst, typ), v) in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim') ^ "\n");
           expand_prim (prim' :: rest, acc)
       | ((proto, dir, None), v) when proto <> Some Vlan && proto <> Some Mpls ->
           if List.mem dir [Some Src; Some Dst; Some Src_or_dst; Some Src_and_dst] && v = Nothing then
             failwith (Aux.error_output_prefix ^ ": " ^
               "Cannot have this type of direction without subject: " ^
               Pcap_syntax_aux.string_of_pcap_expression (Primitive prim))
           else if List.mem dir [Some Inbound; Some Outbound; Some Broadcast; Some Multicast] && v <> Nothing then
             failwith (Aux.error_output_prefix ^ ": " ^
               "Cannot have this type of direction with a subject: " ^
               Pcap_syntax_aux.string_of_pcap_expression (Primitive prim))
           else if proto = None && List.mem dir [Some Broadcast; Some Multicast] && v = Nothing then
             let prim' = ((Some Ether, dir, None), v) in
             Aux.diag_output ("INFO: expand_prim: " ^
               Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
               " --> " ^
               Pcap_syntax_aux.string_of_pcap_expression (Primitive prim') ^ "\n");
             expand_prim (prim' :: rest, acc)
           else if not (List.mem dir [Some Inbound; Some Outbound; Some Broadcast; Some Multicast]) || v <> Nothing then
             let prim' = ((proto, dir, Some Host), v) in
             Aux.diag_output ("INFO: expand_prim: " ^
               Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
               " --> " ^
               Pcap_syntax_aux.string_of_pcap_expression (Primitive prim') ^ "\n");
             expand_prim (prim' :: rest, acc)
           else expand_prim (rest, prim :: acc)
       | ((None, dir, ((Some Host) as typ)), ((MAC_Address _) as v)) ->
           let prim' = ((Some Ether, dir, typ), v) in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim') ^ "\n");
           expand_prim (prim' :: rest, acc)
       | ((None, dir, ((Some Host) as typ)), ((IPv4_Address _) as v))
       | ((None, dir, ((Some Host) as typ)), ((IPv6_Address _) as v))
       | ((None, dir, ((Some Host) as typ)), ((IPv4_Network _) as v))
       | ((None, dir, ((Some Host) as typ)), ((IPv6_Network _) as v))
       | ((None, dir, ((Some Host) as typ)), ((String _) as v))
       | ((None, dir, ((Some Net) as typ)), v) ->
           let prims =
             (*NOTE this phase over-generates expansions -- e.g., having IPv4 with an IPv6 matcher -- but this is later corrected during the simplification phase.*)
             [((Some Ip, dir, typ), v);
              ((Some Ip6, dir, typ), v);
              ((Some Arp, dir, typ), v);
              ((Some Rarp, dir, typ), v)] in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> [" ^
             begin
               List.map (fun prim -> Pcap_syntax_aux.string_of_pcap_expression (Primitive prim)) prims
               |> String.concat "; "
             end ^
             "]" ^ "\n");
           expand_prim (prims @ rest, acc)
       | ((None, dir, ((Some Port_type) as typ)), ((Port _) as v)) ->
           let prims =
             [((Some Tcp, dir, typ), v);
              ((Some Udp, dir, typ), v);
              ((Some Sctp, dir, typ), v)] in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> [" ^
             begin
               List.map (fun prim -> Pcap_syntax_aux.string_of_pcap_expression (Primitive prim)) prims
               |> String.concat "; "
             end ^
             "]" ^ "\n");
           expand_prim (prims @ rest, acc)

       | ((None, dir, ((Some Portrange) as typ)), ((Port_Range _) as v)) ->
           let prims =
             [((Some Tcp, dir, typ), v);
              ((Some Udp, dir, typ), v);
              ((Some Sctp, dir, typ), v)] in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> [" ^
             begin
               List.map (fun prim -> Pcap_syntax_aux.string_of_pcap_expression (Primitive prim)) prims
               |> String.concat "; "
             end ^
             "]" ^ "\n");
           expand_prim (prims @ rest, acc)

       | ((_, _, _), _) ->
           (*Nothing else to expand at the head, so move
             to the rest*)
           Aux.diag_output ("INFO: expand_prim no : " ^ Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^ "\n");
           expand_prim (rest, prim :: acc)
       end

let rec expand_primitives (e : pcap_expression) : pcap_expression =
  match e with
  | Primitive p ->
      begin
      match expand_prim ([p], []) with
      | ([], prims) ->
        begin
        match prims with
        | [] -> failwith (Aux.error_output_prefix ^ ": " ^ "Impossible")
        | [p] -> Primitive p
        | _ -> Or (List.map (fun p -> Primitive p) prims)
        end
      | _ -> failwith (Aux.error_output_prefix ^ ": expand_primitives: " ^ "Unsupported result from expand_prim")
      end
  | True
  | False
  | Atom _ -> e
  | Not e' -> Not (expand_primitives e')
  | And pes -> And (List.map expand_primitives pes)
  | Or pes -> Or (List.map expand_primitives pes)


(** Propagated primitives through connectives **)

(*if P * string * string .. then replicate P (where "*" is AND or OR);
  do this early when processing a pcap expression.*)
(* FIXME Point out resolution of conflict -- if a machine were called 'ftp-data' for example, since were it not for conn_expand then that string would be treated as a hostname by default*)
let rec conn_expand (e : pcap_expression) : pcap_expression =
  match e with
  | Primitive _
  | True
  | False
  | Atom _ -> e
  | Not e' -> Not (conn_expand e')
  | And pes -> And (List.map conn_expand (conn_scan pes))
  | Or pes -> Or (List.map conn_expand (conn_scan pes))

and conn_scan (es : pcap_expression list) : pcap_expression list =
  match es with
  | []
  | [_] -> es
  | (((Primitive ((proto_opt, dir_opt, typ_opt), v)) as pe1) ::
     ((Primitive ((None, None, typ_opt'), String s)) as pe2) :: result) when typ_opt' = None || typ_opt' = typ_opt ->
       let v' =
         match v with
         | Port _ -> Port s
         (*FIXME there might be other kinds of values, not yet supported.
            also need to get this working "src 192.168.1.1 or 192.168.1.2"*)
         | String _ -> String s
         | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "TODO: conn_scan") in
       let pe2' = (Primitive ((proto_opt, dir_opt, typ_opt), v')) in
       Aux.diag_output ("INFO: conn_scan: " ^
         Pcap_syntax_aux.string_of_pcap_expression pe1 ^
         " :: " ^
         Pcap_syntax_aux.string_of_pcap_expression pe2 ^
         " --> " ^
         Pcap_syntax_aux.string_of_pcap_expression pe2' ^ "\n");
       pe1 :: (conn_scan (pe2' :: result))
  | (((Primitive ((proto_opt, dir_opt, typ_opt), v)) as pe1) ::
     ((Primitive ((None, None, typ_opt'), v')) as pe2) :: result) when typ_opt' = None || typ_opt' = typ_opt ->
       (*FIXME DRY wrt previous case*)
       let pe2' = (Primitive ((proto_opt, dir_opt, typ_opt), v')) in
       Aux.diag_output ("INFO: conn_scan: " ^
         Pcap_syntax_aux.string_of_pcap_expression pe1 ^
         " :: " ^
         Pcap_syntax_aux.string_of_pcap_expression pe2 ^
         " --> " ^
         Pcap_syntax_aux.string_of_pcap_expression pe2' ^ "\n");
       pe1 :: (conn_scan (pe2' :: result))
  | (Primitive _ :: result)
  | (True :: result)
  | (False :: result)
  | (Atom _ :: result) -> List.hd es :: conn_scan result
  | (Not _ :: result)
  | (And _ :: result) ->
      (conn_expand (List.hd es)) :: conn_scan result
  | (Or _ :: result) ->
      (conn_expand (List.hd es)) :: conn_scan result


(** Implicit clauses **)

let order_proto_pair (proto1 : proto) (proto2 : proto) : (proto * proto) option =
  let o1 = Layering.layer_of_proto proto1 in
  let o2 = Layering.layer_of_proto proto2 in
  if o1 < o2 then Some (proto1, proto2)
  else if o2 < o1 then Some (proto2, proto1)
  else None

(*e.g., Ip and Arp don't meet, but Tcp and Ip meet at Tcp*)
let meet (proto1 : proto) (proto2 : proto) : proto option =
  Aux.diag_output ("  meet: " ^
   Pcap_syntax_aux.string_of_proto proto1 ^ " vs " ^
   Pcap_syntax_aux.string_of_proto proto2 ^ "\n");
  if proto1 = proto2 then Some proto1
  else
  match order_proto_pair proto1 proto2 with
  | None -> None
  | Some (p1, p2) ->
      begin
        match p1, p2 with
        | Ether, p -> Some p
        | Ip, p when p = Tcp || p = Udp || p = Sctp || p = Icmp -> Some p
        | Ip6, p when p = Tcp || p = Udp || p = Sctp || p = Icmp6 -> Some p
        | Vlan, p
        | Mpls, p -> Some p
        | _ -> None
      end

let is_unexpanded_proto_pe (pe : pcap_expression) : bool =
  match pe with
  | Primitive ((Some _, None, None), Nothing) -> true
  | _ -> false

let rec implicit_expand_pe (pe : pcap_expression) : pcap_expression =
  let process_connective (f : pcap_expression list -> pcap_expression)
   (pes : pcap_expression list) : pcap_expression =
    List.map implicit_expand_pe pes
    |> f in
  match pe with
  | True
  | False -> pe
  | Not (Primitive _ as pe')
  | Not (Atom _ as pe') -> Not (implicit_expand pe')
  | Primitive _
  | Atom _ -> implicit_expand pe
  | Not _ -> pe (*A future phase will normalise this*)
  | Or pes -> Or (List.map implicit_expand_pe pes)
  | And pes -> process_connective (fun pes -> And pes) pes

(*This adds primitives that characterise a given primitive -- make clear it's implicit dependencies, such as which protocols a primitive can depend on, and those protocols' dependencies, etc.*)
and implicit_expand (pe : pcap_expression) : pcap_expression =
      begin
        let protos =
          protos_in_pe ~comprehensive:false pe StringSet.empty
          |> protos_of_strings in
        let _ =
          Aux.diag_output ("INFO: implicit_expand: " ^
           Pcap_syntax_aux.string_of_pcap_expression pe ^
           " protos_in_pe: [" ^
           begin
             Names.ProtoSet.fold (fun proto acc ->
               string_of_proto proto :: acc
             ) protos []
             |> String.concat "; "
           end ^
           "]" ^ "\n") in
        let _, meet = ProtoSet.fold (fun proto acc ->
              match acc with
              | (0, None) -> (1, Some proto)
              | (n, None) -> (n + 1, None)
              | (n, Some other_proto) ->
                  begin
                   match meet proto other_proto with
                   | None -> (n + 1, None)
                   | Some meet -> (n + 1, Some meet)
                  end)
            protos (0, None) in
        let _ =
          Aux.diag_output ("INFO: implicit_expand: " ^
           Pcap_syntax_aux.string_of_pcap_expression pe ^
           " meet: [" ^
           begin
             match meet with
             | None -> "None"
             | Some meet -> "Some " ^ string_of_proto meet
           end ^
           "]" ^ "\n") in
        match meet with
        | None ->
            if ProtoSet.is_empty protos then
              pe
            else
              begin
              match pe with
              | Primitive _
              | True
              | False
              | Atom _
              | Not _(*pe' FIXME recurse through pe'?*) ->
                failwith (Aux.error_output_prefix ^ ": " ^ "No meet computed")
              | Or pes ->
                Or (List.map implicit_expand_pe pes)
              | And pes ->
                And (List.map implicit_expand_pe pes)
              end
        | Some meet ->
            let deps = Layering.deps_of_proto meet
             |> List.map (fun proto ->
               set_proto proto empty
               |> set_typ Proto
               |> set_value (Escaped_String (string_of_proto meet))
               |> (fun prim -> Primitive prim)
               |> implicit_expand_pe) in

            let disjuncts =
              if is_unexpanded_proto_pe pe then
                (*The primitive will have been expanded out by deps, so no need to include "pe" in the resulting formula.*)
                deps
              else List.map (fun dep -> And [dep; pe]) deps in

            if disjuncts = [] then
              (*This means there are no further dependencies,
                so end with what we have*)
              pe
            else
              begin
                begin
                Aux.diag_output ("INFO: implicit_expand: " ^
                 Pcap_syntax_aux.string_of_pcap_expression pe ^
                 " dependencies: [" ^
                 begin
                   List.map Pcap_syntax_aux.string_of_pcap_expression deps
                   |> String.concat "; "
                 end ^
                 "]" ^ "\n")
                end;
                Or disjuncts
                |> Simplifire.simplify_pcap_singleton_connectives
              end
      end

let rec push_neg_pe (pe : pcap_expression) : pcap_expression =
  match pe with
  | Primitive _
  | True
  | False
  | Atom _ -> Not pe
  | Not pe' -> pe'
  | And pes -> Or (List.map push_neg_pe pes)
  | Or pes -> And (List.map push_neg_pe pes)

let rec nnf_pe (pe : pcap_expression) : pcap_expression =
  match pe with
  | Primitive _
  | True
  | False
  | Atom _ -> pe
  | Not pe' -> push_neg_pe pe'
  | And pes -> And (List.map nnf_pe pes)
  | Or pes -> Or (List.map nnf_pe pes)

let nnf_pcap (pe : pcap_expression) : pcap_expression =
  Simplifire.trampoline "nnf_pcap" nnf_pe pe
(*
let rec dnf_expand_pe (pe : pcap_expression) : pcap_expression =
  match pe with
  | Primitive _
  | True
  | False
  | Atom _ -> pe
  | Not pe' -> push_neg_pe pe'
  | And pes -> And (List.map disj_expand pes)
  | Or pes -> Or (List.map dnf_expand_pe pes)
*)
