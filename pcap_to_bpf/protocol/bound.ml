(*
  Copyright Hyunsuk Bang, October 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: inbound and outbound
*)

open Util
open Inst

let inbound () =
  if !Config.linux_bpf then
    Some {
      code = [Ldh (Off (-4092))];
      cond = Jeq (Hexj 0x4);
      jt = ret_true;
      jf = ret_false;
    }
  else
    abort_bpf_gen "need -linux to use outbound"

let outbound () =
  if !Config.linux_bpf then
    Some {
      code = [Ldh (Off (-4092))];
      cond = Jeq (Hexj 0x4);
      jt = ret_true;
      jf = ret_false;
    }
  else
    abort_bpf_gen "need -linux to use inbound"