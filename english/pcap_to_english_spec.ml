(*
  Copyright Marelle Leon, April 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: English spec for translating pcap expressions to English
*)

open Pcap_syntax

let protos =
  [
    (Ether, "ethernet");
    (Ip, "IPv4");
    (Ip6, "IPv6");
    (Arp, "arp");
    (Rarp, "rarp");
    (Tcp, "tcp");
    (Udp, "udp");
    (Sctp, "sctp");
    (Vlan, "vlan");
    (Mpls, "mpls");
    (Icmp, "icmp");
    (Icmp6, "icmp6");
  ]

let dirs =
  [
    (Src, "source");
    (Dst, "destination");
    (Src_or_dst, "source or destination");
    (Src_and_dst, "source and destination");
    (Inbound, "inbound");
    (Outbound, "outbound");
    (Broadcast, "broadcast");
    (Multicast, "multicast");
  ]

let typs =
  [
    (Host, "host");
    (Net, "network");
    (Port_type, "port");
    (Portrange, "port range");
    (Proto, "proto");
    (Protochain, "protochain");
    (Gateway, "gateway");
    (* (Less, "less");
       (Greater, "greater") *)
  ]

let engl_string_of_proto p = List.assoc p protos
let engl_string_of_dir d = List.assoc d dirs
let engl_string_of_typ t = List.assoc t typs
