(*
  Copyright Marelle Leon, April 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module:  Stringifying English expressions
*)

open English_syntax

let string_of_number n =
  match n with
  | "1" -> "first"
  | "2" -> "second"
  | "3" -> "third"
  | "4" -> "fourth"
  | "5" -> "fifth"
  | "6" -> "sixth"
  | "7" -> "seventh"
  | "8" -> "eighth"
  | _ -> n ^ "th" (* FIXME generates "21th", don't make it do "11st" *)

let rec string_of_term = function
  | Phrase s -> s
  | UnidentifiedString s -> s
  | With_term (name, left, right) -> left ^ " with a " ^ name ^ " of " ^ right
  | OfSize_term (size_name, unit_name, left, right) ->
      left ^ " of " ^ size_name ^ " " ^ right ^ " " ^ unit_name
  | OrdinalReference
      (number, ordinal_name, collection_name, collection_type_name) ->
      "the" ^ " " ^ string_of_number number ^ " " ^ ordinal_name ^ " "
      ^ "of the" ^ " " ^ collection_name ^ " " ^ collection_type_name
  | TermList ts -> "[" ^ String.concat ", " (List.map string_of_term ts) ^ "]"

let string_of_arith_val a =
  match a with
  | Arithmetic (s, []) -> "{ " ^ s ^ " }"
  | Arithmetic (s, vars) ->
      let is_left_paren s =
        if s = "" then false else s.[String.length s - 1] = '('
      in
      let is_right_paren s = if s = "" then false else s.[0] = ')' in
      let var_strs =
        List.map
          (fun (l, r_s, is_it_var) ->
            (if is_it_var then "# it" else "# " ^ string_of_term l)
            ^
            if r_s = "" then ""
            else if is_right_paren r_s then r_s
            else " " ^ r_s)
          vars
      in
      "{"
      ^ (if s = "" then "" else " " ^ s)
      ^ (if is_left_paren s then "" else " ")
      ^ String.concat " " var_strs ^ " " ^ "}"

let string_of_clause logically_positive c =
  match c with
  | OfType t -> (if logically_positive then "of type" else "not of type") ^ " " ^ string_of_term t
  | ThatIs (t1, t2) ->
      string_of_term t1 ^ " "
      ^ (if logically_positive then "that is" else "that is not")
      ^ " " ^ string_of_term t2
  | IsThatOf (ts, (TermList _ as t)) ->
      "a" ^ " "
      ^ String.concat " " (List.map string_of_term ts)
      ^ " "
      ^ (if logically_positive then "is one of" else "is not one of")
      ^ " " ^ string_of_term t
  | IsThatOf (ts, t) ->
      "a" ^ " "
      ^ String.concat " " (List.map string_of_term ts)
      ^ " "
      ^ (if logically_positive then "is" else "is not")
      ^ " " ^ string_of_term t
  | ThatHas (t1, ts, (TermList _ as t2)) ->
      string_of_term t1 ^ " "
      ^ (if logically_positive then "that has a" else "that does not have a")
      ^ " "
      ^ String.concat " " (List.map string_of_term ts)
      ^ " "
      ^ (if logically_positive then "which is one of"
        else "which is not one of")
      ^ " " ^ string_of_term t2
  | ThatHas (t1, ts, t2) ->
      string_of_term t1 ^ " "
      ^ (if logically_positive then "that has a" else "that does not have a")
      ^ " "
      ^ String.concat " " (List.map string_of_term ts)
      ^ " " ^ "of" ^ " " ^ string_of_term t2
  | Relation_clause (Phrase comp, left, right) ->
      string_of_arith_val left ^ " " ^ comp ^ " " ^ string_of_arith_val right
  | ExaminingRelation_clause (Phrase comp, it_var, left, right) ->
      let it_str = string_of_term it_var in
      "examining" ^ " " ^ it_str ^ ":" ^ " " ^ string_of_arith_val left ^ " "
      ^ comp ^ " " ^ string_of_arith_val right

let rec size_of_engl_expression = function
  | Clause c -> 1
  | And_expr es ->
      List.fold_right (fun e ct -> ct + size_of_engl_expression e) es 0
  | Or_expr es ->
      List.fold_right (fun e ct -> ct + size_of_engl_expression e) es 0
  | Not_expr e -> size_of_engl_expression e
  | True_expr -> 1
  | False_expr -> 1

let rec sized_enbracket_ee ee =
  let ee_s = string_of_engl_expression ee in
  if size_of_engl_expression ee > 1 then "(" ^ ee_s ^ ")" else ee_s

and string_of_engl_expression = function
  | Clause c -> string_of_clause true c
  | And_expr es -> String.concat ", and " (List.map sized_enbracket_ee es)
  | Or_expr es -> String.concat ", or " (List.map sized_enbracket_ee es)
  | Not_expr (Clause c) -> string_of_clause false c
  | Not_expr e -> "not (" ^ string_of_engl_expression e ^ ")"
  | True_expr -> "TRUE"
  | False_expr -> "FALSE"
