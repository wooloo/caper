(*
  Copyright Marelle Leon, April 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module:  Translating Pcap expressions into English expressions
*)

open Pcap_to_english_spec
open English_syntax
open Pcap_syntax
open Pcap_syntax_aux
open Angstrom

let engl_term_of_value_atom v =
  let proto_engl_string_of_pcap_string_opt s =
    (match s with
    | "ether" -> Some Ether
    | "ip" -> Some Ip
    | "ip6" -> Some Ip6
    | "arp" -> Some Arp
    | "rarp" -> Some Rarp
    | "tcp" -> Some Tcp
    | "udp" -> Some Udp
    | "sctp" -> Some Sctp
    | "vlan" -> Some Vlan
    | "mpls" -> Some Mpls
    | "icmp" -> Some Icmp
    | "icmp6" -> Some Icmp6
    | _ -> None)
    |> Option.map engl_string_of_proto
  in
  let engl_str_of_ipv4_network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) =
    let net_str =
      string_of_id_atom (IPv4_Network (i1, i2_opt, i3_opt, i4_opt, None))
    in
    match mask_size_opt with
    | Some mask_size ->
        OfSize_term ("length", "bits", net_str, string_of_int mask_size)
    | None -> Phrase net_str
  in
  let engl_str_of_ipv4_network_masked (i1, i2_opt, i3_opt, i4_opt, mask) =
    let net_str =
      string_of_id_atom (IPv4_Network (i1, i2_opt, i3_opt, i4_opt, None))
    in
    With_term ("mask", net_str, mask)
  in
  match v with
  | Escaped_String escaped_s -> (
      match proto_engl_string_of_pcap_string_opt escaped_s with
      | Some s -> Phrase s
      | None -> Phrase escaped_s)
  | IPv4_Network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) ->
      engl_str_of_ipv4_network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt)
  | IPv4_Network_Masked (i1, i2_opt, i3_opt, i4_opt, mask) ->
      engl_str_of_ipv4_network_masked (i1, i2_opt, i3_opt, i4_opt, mask)
  | _ -> Phrase (string_of_id_atom v)

let engl_clause_of_primitive (p : primitive) : (clause, string) result =
  let fld, v = p in
  let value = engl_term_of_value_atom v in
  match fld with
  | Some p, Some d, Some t ->
      Ok
        (ThatHas
           ( Phrase (engl_string_of_proto p),
             [ Phrase (engl_string_of_dir d); Phrase (engl_string_of_typ t) ],
             value ))
  | Some p, None, Some t ->
      Ok
        (ThatHas
           ( Phrase (engl_string_of_proto p),
             [ Phrase (engl_string_of_typ t) ],
             value ))
  | Some p, Some d, None -> (
      match d with
      | Broadcast | Multicast ->
          Ok
            (ThatIs
               (Phrase (engl_string_of_proto p), Phrase (engl_string_of_dir d)))
      | _ ->
          Ok
            (ThatHas
               ( Phrase (engl_string_of_proto p),
                 [ Phrase (engl_string_of_dir d) ],
                 value )))
  | Some p, None, None -> Ok (OfType (Phrase (engl_string_of_proto p)))
  | None, Some d, Some t ->
      Ok
        (IsThatOf
           ( [ Phrase (engl_string_of_dir d); Phrase (engl_string_of_typ t) ],
             value ))
  | None, None, Some t -> (
      match t with
      | Less -> (
          match value with
          | Phrase val_s ->
              Ok
                (Relation_clause
                   ( Phrase "is less than or equal to",
                     Arithmetic ("len", []),
                     Arithmetic (val_s, []) )))
      | Greater -> (
          match value with
          | Phrase val_s ->
              Ok
                (Relation_clause
                   ( Phrase "is greater than or equal to",
                     Arithmetic ("len", []),
                     Arithmetic (val_s, []) )))
      | _ -> Ok (IsThatOf ([ Phrase (engl_string_of_typ t) ], value)))
  | None, Some d, None -> (
      match d with
      | Inbound | Outbound -> Ok (OfType (Phrase (engl_string_of_dir d)))
      | _ -> Ok (IsThatOf ([ Phrase (engl_string_of_dir d) ], value)))
  | _ -> Error ("single value with no field: '" ^ string_of_primitive p ^ "'")

let engl_clause_of_primitive_disjunction (disjunction : pcap_expression list) :
    clause option =
  let rec flatten_values es =
    List.fold_right
      (fun p vals_opt ->
        match vals_opt with
        | Some vs -> (
            match p with
            | Primitive (fld, v) -> (
                let value = engl_term_of_value_atom v in
                match fld with
                | None, None, None -> Some (value :: vs)
                | _ -> None)
            | Or es' -> (
                match flatten_values es' with
                | Some vs' -> Some (vs' @ vs)
                | None -> None)
            | _ -> None)
        | None -> None)
      es (Some [])
  in
  match disjunction with
  | [] | [ _ ] -> None
  | Primitive prim :: es -> (
      let singe_values = flatten_values es in
      match singe_values with
      | Some vs -> (
          let fld, v = prim in
          let value = engl_term_of_value_atom v in
          match engl_clause_of_primitive prim with
          | Ok (IsThatOf (left, _)) ->
              Some (IsThatOf (left, TermList (value :: vs)))
          | Ok (ThatHas (left, middle, _)) ->
              Some (ThatHas (left, middle, TermList (value :: vs)))
          | Ok _ -> None
          | Error _ -> None)
      | None -> None)
  | _ -> None

let engl_clause_of_rel_expr (r : rel_expr) : clause =
  let rec expr_contains e_out e_in =
    match e_out with
    | Identifier _ | Nat_Literal _ | Hex_Literal _ | Len | Packet_Access _ ->
        e_out = e_in
    | Plus es | Times es | Binary_And es | Binary_Or es | Binary_Xor es ->
        not
          (List.for_all (( = ) false)
             (List.map (fun e -> expr_contains e e_in) es))
    | Minus (e1, e2)
    | Quotient (e1, e2)
    | Mod (e1, e2)
    | Shift_Right (e1, e2)
    | Shift_Left (e1, e2) ->
        expr_contains e1 e_in || expr_contains e2 e_in
  in
  let ordinal_reference_of_expr s e nb_opt =
    let ordinal_name_string_of_no_bytes = function
      | One -> "byte"
      | Two -> "2 bytes"
      | Four -> "4 bytes"
    in
    let collection_name = engl_string_of_proto (proto_of_string s) in
    let ordinal_name_str =
      match nb_opt with
      | None -> ordinal_name_string_of_no_bytes One
      | Some nb -> ordinal_name_string_of_no_bytes nb
    in
    OrdinalReference
      (string_of_expr e, ordinal_name_str, collection_name, "header")
  in
  let arith_val_of_expr (e : expr) : arith_val =
    (*
         if expr is a single packet access,
           replace it with an ordinal reference
         else,
           leave the whole thing as an unbroken string
    *)
    match e with
    | Packet_Access (s, e', nb_opt) ->
        Arithmetic ("", [ (ordinal_reference_of_expr s e' nb_opt, "", false) ])
    | _ -> Arithmetic (string_of_expr e, [])
  in
  let arith_val_of_expr_with_it_variable (it_term : term) (it_str : string)
      (e : expr) : arith_val =
    (* find instances of it_str and break the expr string into pieces *)
    let build_arith : arith_val t =
      let it_str_token = English_parsing.keyword it_str in
      let any_token_not_paren =
        English_parsing.token
          (take_while1 (fun x ->
               English_parsing.is_any_tok x && (not (x = '(')) && not (x = ')')))
      in
      let paren_token =
        English_parsing.token
          (take_while1 (fun x ->
               English_parsing.is_any_tok x && (x = '(' || x = ')')))
      in
      let rec concat_paren_str strs =
        let is_left_paren s =
          if s = "" then false else s.[String.length s - 1] = '('
        in
        let is_right_paren s = if s = "" then false else s.[0] = ')' in
        match strs with
        | [] -> ""
        | [ x ] -> x
        | x :: y :: rest when is_left_paren x || is_right_paren y ->
            let rest_s = concat_paren_str rest in
            let rest_s = if rest_s = "" then "" else " " ^ rest_s in
            (x ^ y) ^ if rest = [] then "" else rest_s
        | x :: rest ->
            let rest_s = concat_paren_str rest in
            let rest_s = if rest_s = "" then "" else " " ^ rest_s in
            x ^ rest_s
      in
      let until_it_str =
        English_parsing.rest_of_matches_before
          (any_token_not_paren <|> paren_token)
          it_str_token
        >>= fun strs -> return (concat_paren_str strs)
      in
      let it_str_until_next =
        it_str_token *> until_it_str >>= fun s -> return (it_term, s, true)
      in
      until_it_str >>= fun s ->
      many it_str_until_next >>= fun its -> return (Arithmetic (s, its))
    in
    let parse_arithmetic = parse_string ~consume:Prefix build_arith in
    let e_str = string_of_expr e in
    Result.get_ok (parse_arithmetic e_str)
  in
  let relation_clause (comp : string) (e1 : expr) (e2 : expr) : clause =
    match (e1, e2) with
    | Packet_Access (s, e, nb_opt), _ when expr_contains e2 e1 ->
        (* 'it' variable must be contained in other side's expression *)
        let it_str = string_of_expr e1 in
        let it_term = ordinal_reference_of_expr s e nb_opt in
        ExaminingRelation_clause
          ( Phrase comp,
            it_term,
            arith_val_of_expr_with_it_variable it_term it_str e1,
            arith_val_of_expr_with_it_variable it_term it_str e2 )
    | _, Packet_Access (s, e, nb_opt) when expr_contains e1 e2 ->
        (* 'it' variable must be contained in other side's expression *)
        let it_str = string_of_expr e2 in
        let it_term = ordinal_reference_of_expr s e nb_opt in
        ExaminingRelation_clause
          ( Phrase comp,
            it_term,
            arith_val_of_expr_with_it_variable it_term it_str e1,
            arith_val_of_expr_with_it_variable it_term it_str e2 )
    | _ ->
        (* no 'it' variable *)
        Relation_clause (Phrase comp, arith_val_of_expr e1, arith_val_of_expr e2)
  in
  match r with
  | Gt (left, right) -> relation_clause "is greater than" left right
  | Lt (left, right) -> relation_clause "is less than" left right
  | Geq (left, right) ->
      relation_clause "is greater than or equal to" left right
  | Leq (left, right) -> relation_clause "is less than or equal to" left right
  | Eq (left, right) -> relation_clause "is equal to" left right
  | Neq (left, right) -> relation_clause "does not equal" left right

let rec engl_expression_result_of_pcap_expression (pcap : pcap_expression) :
    (engl_expression, string) result =
  let list_expression_result expr_f es =
    Result.map expr_f
      (List.fold_right
         (fun e rest ->
           match rest with
           | Ok xs ->
               Result.map
                 (fun x -> x :: xs)
                 (engl_expression_result_of_pcap_expression e)
           | Error err -> Error err)
         es (Ok []))
  in
  let e_res =
    match pcap with
    | Primitive p -> Result.map (fun x -> Clause x) (engl_clause_of_primitive p)
    | Atom r -> Ok (Clause (engl_clause_of_rel_expr r))
    | And es -> list_expression_result (fun x -> And_expr x) es
    | Or es -> (
        match engl_clause_of_primitive_disjunction es with
        | Some c -> Ok (Clause c)
        | None -> list_expression_result (fun x -> Or_expr x) es)
    | Not e ->
        Result.map
          (fun x -> Not_expr x)
          (engl_expression_result_of_pcap_expression e)
    | True -> Ok True_expr
    | False -> Ok False_expr
  in
  match e_res with
  | Ok e -> Ok e
  | Error err -> Error ("Semantic error: " ^ err)
